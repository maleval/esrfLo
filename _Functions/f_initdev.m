%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DiNi(2022) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% Function that Initiate DiniCOM %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% M.Spitoni - Created on 09/02/2023 %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%input: ()
%output: myserial 
%output: SerialPortDini

function [myserial,SerialPortDini] = f_initdev() %Init 
    clear SerialPortDini
    clear myserial
    disp("Init DiNi ongoing...")
    myseriaportllist = serialportlist("available");
    myserial = myseriaportllist(end);
    SerialPortDini = serialport(myserial,9600);
    configureTerminator(SerialPortDini,"CR/LF");
    SerialPortDini.ByteOrder = "big-endian";
    disp("Init DiNi done!")
end




% arguments
%     filepath (1,1) string {mustBeFile} % Set constrains of format, vartype and filetype
%     type (1,1) string  % Set constrains of format, vartype
% end
